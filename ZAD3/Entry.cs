using System;
using System.Collections.Generic;

namespace ZAD3
{
    class Entry
    {
        static void Main(string[] args)
        {
            List<IRentable> rentlist = new List<IRentable>();
            Video video = new Video("Arctic Monkeys - Do I Wanna Know?");
            Book book = new Book("Fahrenheit 451");
            rentlist.Add(video);
            rentlist.Add(book);
            RentingConsolePrinter printer = new RentingConsolePrinter();
            printer.DisplayItems(rentlist);
            printer.PrintTotalPrice(rentlist);
        }
    }
}
