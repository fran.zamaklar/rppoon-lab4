using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD3
{
        interface IRentable
        {
            String Description { get; }
            double CalculatePrice();
        }
}
