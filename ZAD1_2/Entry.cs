using System;

namespace ZAD1_2
{
    class Entry
    {
        static void Main(string[] args)
        {
            Dataset file = new Dataset(@"C:\Users\Lenovo\Documents\Filename.csv");
            Adapter adapter = new Adapter(new Analyzer3rdParty());
            double[][] matrix = adapter.ConvertData(file);
            double[] averages = adapter.CalculateAveragePerColumn(file);
            for(int i = 0; i < averages.Length; i++)
            {
                Console.WriteLine(averages[i]);
            }
            Adapter.ToString(matrix);
        }
    }
}
