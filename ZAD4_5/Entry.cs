using System;
using System.Collections.Generic;

namespace ZAD4_5
{
    class Entry
    {
        static void Main(string[] args)
        {
            //Zad 4

            List<IRentable> rentlist = new List<IRentable>();
            Video video = new Video("Arctic Monkeys - Do I Wanna Know?");
            Book book = new Book("Fahrenheit 451");
            rentlist.Add(video);
            rentlist.Add(book);
            RentingConsolePrinter printer = new RentingConsolePrinter();
            printer.DisplayItems(rentlist);
            printer.PrintTotalPrice(rentlist);
            HotItem trendingvideo = new HotItem((new Video("London Grammar - If You Wait")));
            HotItem trendingbook = new HotItem(new Book("Kradljivica knjiga"));
            rentlist.Add(trendingvideo);
            rentlist.Add(trendingbook);
            printer.DisplayItems(rentlist);
            printer.PrintTotalPrice(rentlist);
            Console.WriteLine();

            //Zad 5 

            List<IRentable> flashSale = new List<IRentable>();
            DiscountedItem discountedvid = new DiscountedItem(video);
            DiscountedItem discountedtvid = new DiscountedItem(trendingvideo);
            DiscountedItem discountedbook = new DiscountedItem(book);
            DiscountedItem discountedtbook = new DiscountedItem(trendingbook);
            flashSale.Add(discountedvid);
            flashSale.Add(discountedtvid);
            flashSale.Add(discountedbook);
            flashSale.Add(discountedtbook);
            printer.DisplayItems(flashSale);
            printer.PrintTotalPrice(flashSale);

        }
    }
}
