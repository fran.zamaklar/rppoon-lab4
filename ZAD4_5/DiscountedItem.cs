using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD4_5
{
    class DiscountedItem : RentableDecorator
    {
        private  double DiscountedItemBonus = 25;
        public DiscountedItem(IRentable rentable) : base(rentable) { }
        public override double CalculatePrice()
        {
            return base.CalculatePrice() * (1 - (this.DiscountedItemBonus/100));
        }

        public void setDiscount(double discount)
        {
            DiscountedItemBonus = discount;
        }

        public override String Description
        {
            get
            {
                return base.Description + " - currently at " + DiscountedItemBonus + "% off";
            }
        }
    }
}
