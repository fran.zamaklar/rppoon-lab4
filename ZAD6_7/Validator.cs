using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD6_7
{
    class Validatior : IRegistrationValidator
    {
        EmailValidator emailValidator;
        PasswordValidator passwordValidator;

        public Validatior(int minLength)
        {
            emailValidator = new EmailValidator();
            passwordValidator = new PasswordValidator(minLength);
        }

        public bool IsUserEntryValid(UserEntry entry)
        {
            return emailValidator.IsValidEmail(entry.Email) && passwordValidator.IsValidPassword(entry.Password);
        }
    }
}
