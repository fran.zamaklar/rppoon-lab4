using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD6_7
{
    class EmailValidator
    {

        public bool IsValidEmail(String candidate)
        {
            if (String.IsNullOrEmpty(candidate))
            {
                return false;
            }
            return HasAt(candidate) && EndsWith(candidate);
        }

        public bool HasAt(string candidate)
        {
            return candidate.Contains("@");
        }

        public bool EndsWith(string candidate)
        {
            return candidate.EndsWith(".com") || candidate.EndsWith("hr");
        }
    }
}
